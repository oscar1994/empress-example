const cors = require('cors');
const express = require('express');

const app = express();

const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');


//Settings
const config = require('./config/config')
app.set('superSecret', config.secret);
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', userRoutes);

app.use('/login', authRoutes);


app.listen(app.get('port'));