const mysql = require('mysql2');

exports.Connection = mysql.createConnection({
    host: "localhost",
    user: "isw811_user",
    password: "secret",
    database: "software"
});